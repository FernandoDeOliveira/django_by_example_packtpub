
# django by example - packtpub

course of django by [packtpub](https://www.packtpub.com/)


## Overview
- Learn Django by building four fully-functional, real-world web applications from scratch
- Develop powerful web applications quickly using the best coding practices
- Integrate other technologies into your application with clear, step-by-step explanations and comprehensive example code

## Objectives
- Build practical real-world web applications with Django
- Use Django with other technologies such as Redis, Celery, Solr, and Memcached
- Develop pluggable Django applications to create advanced features
- Optimize your code and use the cache framework
- Add internationalization to your Django projects
- Enhance the user experience using JavaScript and AJAX
- Add social features to your projects
- Build RESTful APIs for your applications

## About
Django is a powerful Python web framework designed to develop web applications quickly, from simple prototypes to large-scale projects. Django encourages clean, pragmatic design, and provides developers with a comprehensive set of tools to build scalable web applications. This video will walk you through the creation of four professional Django projects, teaching you how to solve common problems and implement best practices.

The video begins by showing you how to build a blog application, before moving on to developing a social image bookmarking website, an online shop, and an e-learning platform. You will learn how to build a search engine and implement a user activity stream. Furthermore, you will create a recommendation engine, an e-commerce coupon system, and a content management system.

The video will also teach you how to enhance your applications with AJAX, create RESTful APIs, and setup a production environment for your Django projects. After going through this video, you will have a good understanding of how Django works and how to integrate it with other technologies to build practical, advanced web applications.

Style and Approach
This is an easy-to-follow video to help you build four different production-ready Django projects. Each project focuses on a particular area of the framework and each topic within the projects is explained with practical examples and includes best practice recommendations.